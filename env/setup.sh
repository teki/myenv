#!/bin/sh

# gdb
cp -R ~/myenv/env/.gdb ~
cp ${MYENVPATH}/env/.gdbinit ~

# profile with extension support
cat << EOF > ~/.profile
source ~/myenv/env/environment.sh
if [ -f ~/myenv.d/env/environment.sh ]; then
	source ~/myenv.d/env/environment.sh
fi
EOF

# vimrc
if [[ $(uname) == CYGWIN* ]] ; then
	echo "source ~/myenv/vim_local/vimrc" > ~/.vimrc
	~/myenv/env/setpath.cmd "%HOME%\myenv\bin\w"
else
	echo "source ~/myenv/vim_local/vimrc" > ~/.vimrc
fi

# gitconfig
read -p "Git name: " a
sed "s/nnaammee/$a/" ~/myenv/env/.gitconfig > ~/.gitconfig1
read -p "Git email: " a
sed "s/eemmaaiill/$a/" ~/.gitconfig1 > ~/.gitconfig
rm ~/.gitconfig1

