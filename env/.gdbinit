source ~/.gdb/gdb_stl_utils

define mybt
  set logging off
  set logging file /tmp/tmp_file_gdb_names
  set pagination off
  set logging overwrite on
  set logging redirect on
  set logging on
  thread apply all bt
  set logging off
  set logging redirect off
  set logging overwrite off
  set pagination on
  shell ~/bin/gdb_mybt.py /tmp/tmp_file_gdb_names
  shell rm -f /tmp/tmp_file_gdb_names
end


define wchar_print
        echo "

        set $i = 0
        while (1 == 1)
                set $c = (wchar_t)(($arg0)[$i++])
                if ($c == '\0')
                        loop_break
                end
                printf "%c", $c
        end

        echo "\n
end

define pdatas
  pmap &($arg0.px->m_Data) int 'MediaLib::MediaData'
end

define poffsets
  pmap $arg0 int 'MediaLib::ClipOffset'
end

define ppacket
  pmap $arg0 int 'MediaLib::ClipOffset'
end

document wchar_print
wchar_print <wstr>
Print ASCII part of <wstr>, which is a wide character string of type wchar_t*.
end


future-break __assert_rtn
set scheduler-locking step
