# tools/misc
function path_i()
{
	export PATH="$@":$PATH
}

function path_a()
{
	export PATH=$PATH:"$@"
}

function manpath_a()
{
	export MANPATH=$MANPATH:"$@"
}

function dl_a()
{
	export DYLD_LIBRARY_PATH="$DYLD_LIBRARY_PATH:$@"
}

function ismac()
{
	[ "$(uname)" == "Darwin" ]
}

function iswin()
{
	[[ "$(uname)" == CYGWIN* ]]
}

function askyn()
{
	printf "$1 (Y/n)"
	read yn
	if [ "$yn" == "n" ]
	then
		exit 1
	fi
}

# path

export PYTHONPATH=~/myenv/py
export GOPATH=~/myenv/gocode

path_i ~/myenv/bin

if ismac ; then
	#homebrew
	path_i /usr/local/bin
	#mac specific scripts
	path_a ~/myenv/bin/m
fi

if iswin ; then
	path_a ~/myenv/bin/w
fi

#  prompt
BLACK=0
RED=1
GREEN=2
YELLOW=3
BLUE=4
MAGENTA=5
CYAN=6
WHITE=7
NONE=8

C_PREFIX="\[\e["
C_SUFFIX="m\]"

c=$BLACK;
while [ $c -le $NONE ]; do
    reg=$((30 + $c))
    inv=$((40 + $c))
    C_NORM[$c]="${C_PREFIX}0;$reg$C_SUFFIX"
    C_BOLD[$c]="${C_PREFIX}1;$reg$C_SUFFIX"
    C_NORM_HI[$c]="${C_PREFIX}0;$inv$C_SUFFIX"
    C_BOLD_HI[$c]="${C_PREFIX}1;$inv$C_SUFFIX"
    c=$(($c + 1))
done

prompt_colour=${C_NORM[$RED]}
[[ $(echo $(hostname) | tr '[A-Z]' '[a-z]') =~ bela ]] && prompt_colour=${C_NORM[$GREEN]}
[[ $(echo $(hostname) | tr '[A-Z]' '[a-z]') =~ dot ]] && prompt_colour=${C_NORM[$GREEN]}

export normal_prompt="$prompt_colour\h$os_prompt:\w \u: ${C_NORM[$NONE]}"
export PS1=$normal_prompt
export PROMPT_COMMAND='echo -ne "\033]0;${HOSTNAME%%.*}:${PWD/#$HOME/~} ${USER}\007"'

export HISTCONTROL=ignoredups:erasedups  # no duplicate entries
export HISTSIZE=100000                   # big big history
export HISTFILESIZE=100000               # big big history
shopt -s histappend                      # append to history, don't overwrite it

export ACK_OPTIONS=-a
# user specific
alias ll='ls -Gal'

function es() {
	ko "$(type -p $1)"
}

function koo() {
	xargs ko
}

export EDITOR=vim
export VISUAL=vim
