#!/usr/bin/env python
import sys
import re

f = ["QCoreApplication::exec",
"boost::condition_variable_any::timed_wait" ,
"boost::condition_variable_any::wait" ,
"IcpCompressorComponentDispatch",
"DriverNotificationThreadFunction",
"VideoPreviewThreadFunction",
"VTRControl::asyncSerial",
"_pthread_wqthread",
"CDeckLinkOutput::DriverNotification",
"CDeckLinkOutput::VideoPreview",
"boost::condition_variable::timed_wait",
"boost::condition_variable::timed_wait",
"DVDeviceTerminate",
"DVRunThread",
"ReadSchedulerThreadEntryPoint",
"AIOFileThread",
"UMC::Event::Wait",
"BMD::Socket::Receive",
"AudioConsumerThread",
"AudioProducerThread",
"CStreamingDeviceInput::EventThread",
]

msgproc = re.compile(".*MediaLib::MediaHub::processMessage.*")
msgprocthr = []

a = open(sys.argv[1]).read()
b = re.split("Thread (\d+)",a)
thr=""
ll=1
assrt=""

for i,c in enumerate(b):
	c=c.strip()
	if not c:
		continue
	if i & 1:
		thr = c.strip()
		continue
	m = msgproc.search(c)
	if m:
		msgprocthr.append((thr,m.group(0)))
	elif [x for x in f if x in c]:
		continue
	if "__assert_rtn" in c:
		assrt=thr
	
	print "Thread",thr,c
	print

if assrt:
	print "ASSERT found in thread:",assrt

if len(msgprocthr) > 1:
	print "Possible message deadlock: ", msgprocthr
	

